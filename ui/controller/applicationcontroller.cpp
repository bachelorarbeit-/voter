#include "applicationcontroller.h"

#include <mk-tfhe-voting/exceptions/JsonError.h>
#include <mk-tfhe-voting/exceptions/CoordinatorError.h>
#include <voter/exceptions/ApiKeyError.h>
#include <voter/exceptions/NotRegisteredError.h>
#include <voter/exceptions/GuiWorkerError.h>

#include <utility>
#include <iostream>

#include <cpprest/http_msg.h>


namespace hsr::applicationcontroller {
	ApplicationController::ApplicationController(
			hsr::applicationwindow::ApplicationWindow *applicationWindow,
			std::shared_ptr<hsr::voter::Voter> voter, std::string coordinatorUri) :
			applicationWindow{applicationWindow},
			voter{std::move(voter)},
			coordinatorUri{std::move(coordinatorUri)},
			refreshDispatcher{},
			participationDispatcher{},
			registerDispatcher{},
			votingPackagesDispatcher{},
			voteStatusDispatcher{},
			delayRefreshButtonDispatcher{},
			delayRefreshButtonWorker{delayRefreshButtonDispatcher, 10},
			delayRefreshChoicesDispatcher{},
			delayRefreshChoicesWorker{delayRefreshChoicesDispatcher, 1} {

		applicationWindow->getCoordinatorUri()->set_text(this->coordinatorUri);


		registerWorker = std::make_shared<hsr::genericworker::GenericWorker<bool>>(registerDispatcher, [this] {
			try {
				isCoordinatorNotReachableRegister = false;
				this->voter->getCoordinator().lock()->registerVoter();
				this->applicationWindow->getRefreshTimeLabel()->set_label("");
				return true;
			} catch (web::http::http_exception const &e) {
				std::cout << "Coordinator is not reachable to register\n" << e.what() << "\n\n";
				isCoordinatorNotReachableRegister = true;
			} catch (ApiKeyError const &e) {
				this->applicationWindow->getRefreshTimeLabel()->set_label(e.what());
				std::cout << e.what() << "\n\n";
			}
			return false;
		});

		refreshWorker = std::make_shared<hsr::genericworker::GenericWorker<std::map<unsigned int, VotesText>>>(refreshDispatcher, [this] {
			try {
				isCoordinatorNotReachableGetVotes = false;
				return this->voter->getCoordinator().lock()->getVotesVoter();
			} catch (web::http::http_exception const &e) {
				std::cout << "Coordinator is not reachable to get votes\n" << e.what() << "\n\n";
				isCoordinatorNotReachableGetVotes = true;
			}
			return std::map<unsigned int, VotesText>{};
		});

		participationWorker = std::make_shared<hsr::genericworker::GenericWorker<std::vector<unsigned int>>>(participationDispatcher, [this] {
			try {
				return this->voter->getCoordinator().lock()->getParticipations();
			} catch (web::http::http_exception const &e) {
				std::cout << "Coordinator is not reachable to get participation.\n" << e.what() << "\n\n";
				isCoordinatorNotReachableGetVotes = true;
			} catch (NotRegisteredError const &e) {
				std::cout << "Not registered on coordinator to get participation.\n" << e.what() << "\n\n";
				this->applicationWindow->getStatusVotesButtonServerStatus()->set_label(e.what());
			}
			return std::vector<unsigned int>{};
		});

		votingPackagesWorker = std::make_shared<hsr::genericworker::GenericWorker<std::map<unsigned int, std::shared_ptr<VotingPackage>>>>(votingPackagesDispatcher, [this] {
			try {
				return this->voter->getVotingPackages(voteIds);
			} catch (web::http::http_exception const &e) {
				std::cout << "Coordinator is not reachable to get voting package.\n" << e.what() << "\n\n";
			}
			return std::map<unsigned int, std::shared_ptr<VotingPackage>>{};
		});

		voteStatusWorker = std::make_shared<hsr::genericworker::GenericWorker<std::map<unsigned int, VoteStatusInformation>>>(voteStatusDispatcher, [this] {
			try {
				return this->voter->getCoordinator().lock()->getVoteStatusInformation(this->participationIds);
			} catch (web::http::http_exception const &e) {
				std::cout << "Coordinator is not reachable to get voting package.\n" << e.what() << "\n\n";
			} catch (JsonError const &e) {
				std::cout << e.what() << "\n\n";
			}
			return std::map<unsigned int, VoteStatusInformation>{};
		});

		applicationWindow->setOnStartVote([this] {

			for (auto &checkBox : *this->applicationWindow->getQuestions()) {
				if (checkBox.second.get_active()) {
					voteTextChosenForVote.insert({checkBox.first, voteTextListed.at(checkBox.first)});
					voteTextListed.erase(checkBox.first);
					voteIds.emplace_back(checkBox.first);
				}
			}

			if (isCoordinatorNotReachableRegister) {
				this->applicationWindow->getStartVoteButtonServerStatus()->set_label("Coordinator is not reachable to register");
			} else {
				this->voteAssistantWindow = new hsr::voteassistantwindow::VoteAssistantWindow();
				this->voteAssistantWindow->set_modal(true);
				this->applicationWindow->getStartVoteButtonServerStatus()->set_label("");
				this->voteAssistantController = std::make_shared<hsr::voteassistantcontroller::VoteAssistantController>(
						this->voteAssistantWindow, this->voter, voteTextChosenForVote, votingPackagesWorker, [this]() {
							for (auto &sigBox : disposableSignals) {
								sigBox.disconnect();
							}
							this->applicationWindow->getQuestions()->clear();
							this->applicationWindow->getStartVoteButton()->set_sensitive(false);

							delayRefreshChoicesWorker.run();
						});

				voteAssistantWindow->show();

				votingPackagesWorker->run();
				disposableSignals.emplace_back(votingPackagesDispatcher.connect(sigc::mem_fun(*this->voteAssistantController,
																							  &voteassistantcontroller::VoteAssistantController::on_notification_from_worker_thread_votingPackage)));
			}
		});


		applicationWindow->setOnStatusVotes([this] {
			participationWorker->run();
		});

		applicationWindow->setOnRefreshVote([this] {
			refreshWorker->run();
		});

		registerDispatcher.connect(sigc::mem_fun(*this, &ApplicationController::on_notification_from_worker_thread_register));
		refreshDispatcher.connect(sigc::mem_fun(*this, &ApplicationController::on_notification_from_worker_thread_refresh));
		participationDispatcher.connect(sigc::mem_fun(*this, &ApplicationController::on_notification_from_worker_thread_participation));
		voteStatusDispatcher.connect(sigc::mem_fun(*this, &ApplicationController::on_notification_from_worker_thread_voteStatus));
		delayRefreshButtonDispatcher.connect(sigc::mem_fun(*this, &ApplicationController::on_notification_from_worker_thread_delay_refresh));
		delayRefreshChoicesDispatcher.connect(sigc::mem_fun(*this, &ApplicationController::on_notification_from_worker_thread_delay_choices));

		registerWorker->run();
	}

	ApplicationController::~ApplicationController() {
		on_force_stop_request_get_Votes();
	}

	void ApplicationController::updateRefreshButton() {
		if (refreshWorker->stopped()) {
			delayRefreshButtonWorker.run();
			if (isCoordinatorNotReachableGetVotes) {
				this->applicationWindow->getRefreshTimeLabel()->set_label("Coordinator is not reachable to get elections");
				this->applicationWindow->getRefreshTimeLabel()->get_style_context()->add_class("important_label");
				this->applicationWindow->getRefreshVotesButton()->set_sensitive(true);
			} else {
				this->applicationWindow->getRefreshVotesButton()->set_sensitive(false);
				this->applicationWindow->getRefreshTimeLabel()->get_style_context()->remove_class("important_label");
				std::time_t result = std::time(nullptr);
				this->applicationWindow->getRefreshTimeLabel()->set_label(std::asctime(std::localtime(&result)));
			}
		}
	}

	void ApplicationController::onCheckButtonToggled() {
		this->applicationWindow->getStartVoteButton()->set_sensitive(isElectionChecked());
	}

	int ApplicationController::isElectionChecked() const {
		for (auto &checkBox : *applicationWindow->getQuestions()) {
			if (checkBox.second.get_active()) {
				return true;
			}
		}
		return false;
	}

	void ApplicationController::update_widgets() {
		try {
			voteTextListed = refreshWorker->get_data();
		} catch (GuiWorkerError const &e) {
			std::string errorMessage{"Couldn't get vote data, refresh again."};
			std::cout << errorMessage << "\n" << e.what() << "\n\n";
			this->applicationWindow->getRefreshTimeLabel()->set_label(errorMessage);
		}

		for (auto &element : voteTextListed) {
			if (this->applicationWindow->getQuestions()->find(element.first) == this->applicationWindow->getQuestions()->end()) {
				this->applicationWindow->getQuestions()->insert({element.first, Gtk::CheckButton(element.second.question)});
				this->applicationWindow->getQuestionBox()->add(this->applicationWindow->getQuestions()->at(element.first));

				disposableSignals.emplace_back(this->applicationWindow->getQuestions()->at(element.first).signal_toggled().connect(sigc::mem_fun(*this,
																																				 &ApplicationController::onCheckButtonToggled)));
			}
		}

		this->applicationWindow->getQuestionBox()->show_all_children();
	}

	void ApplicationController::on_notification_from_worker_thread_refresh() {
		updateRefreshButton();
		update_widgets();
	}

	void ApplicationController::on_notification_from_worker_thread_register() {
		try {
			registerWorker->get_data();
			refreshWorker->run();
		} catch (GuiWorkerError const &e) {
			std::string errorMessage{"Couldn't get register data, restart the app."};
			std::cout << errorMessage << "\n" << e.what() << "\n\n";
			this->applicationWindow->getStartVoteButtonServerStatus()->set_label(errorMessage);
		}
	}

	void ApplicationController::on_notification_from_worker_thread_participation() {
		try {
			this->applicationWindow->getStatusVotesButtonServerStatus()->set_label("");
			participationIds = participationWorker->get_data();
			voteStatusWorker->run();
		} catch (GuiWorkerError const &e) {
			std::string errorMessage{"Couldn't get participation data, restart the app."};
			std::cout << errorMessage << "\n" << e.what() << "\n\n";
			this->applicationWindow->getStatusVotesButtonServerStatus()->set_label(errorMessage);
		}
	}

	void ApplicationController::on_notification_from_worker_thread_voteStatus() {
		try {
			auto voteStatus = voteStatusWorker->get_data();
			if (!voteStatus.empty()) {
				this->applicationWindow->getStatusVotesButtonServerStatus()->set_label("");
				this->voteStatusWindow = new hsr::votestatuswindow::VoteStatusWindow();
				this->voteStatusWindow->set_modal(true);
				this->voteStatusController = std::make_shared<hsr::voterstatuscontroller::VoteStatusController>(this->voteStatusWindow, voteStatus);
				this->voteStatusController->showVoteStatusInformation(participationIds);
				this->voteStatusWindow->show();
			} else {
				std::string errorMessage{"Vote Status is empty, click again later on."};
				std::cout << errorMessage << "\n\n";
				this->applicationWindow->getStatusVotesButtonServerStatus()->set_label(errorMessage);
			}
		} catch (GuiWorkerError const &e) {
			std::string errorMessage{"Couldn't get vote status data, click again."};
			std::cout << errorMessage << "\n" << e.what() << "\n\n";
			this->applicationWindow->getStatusVotesButtonServerStatus()->set_label(errorMessage);
		} catch (NotRegisteredError const &e) {
			std::string errorMessage{"You are not registered yet, vote first."};
			std::cout << errorMessage << "\n" << e.what() << "\n\n";
			this->applicationWindow->getStatusVotesButtonServerStatus()->set_label(errorMessage);
		} catch (CoordinatorError const &e) {
			std::string errorMessage{"Coordinator couldn't be reached, try again later."};
			std::cout << errorMessage << "\n" << e.what() << "\n\n";
			this->applicationWindow->getStatusVotesButtonServerStatus()->set_label(errorMessage);
		}
	}

	void ApplicationController::on_notification_from_worker_thread_delay_refresh() {
		try {
			this->delayRefreshButtonWorker.stop_work();
		} catch (GuiWorkerError const &e) {
			std::cout << e.what() << "\n\n";
		}

		this->applicationWindow->getRefreshVotesButton()->set_sensitive(true);
	}

	void ApplicationController::on_notification_from_worker_thread_delay_choices() {
		try {
			this->delayRefreshChoicesWorker.stop_work();
		} catch (GuiWorkerError const &e) {
			std::cout << e.what() << "\n\n";
		}

		refreshWorker->run();
	}

	void ApplicationController::on_force_stop_request_get_Votes() {
		refreshWorker->stop_work();
		registerWorker->stop_work();
		delayRefreshButtonWorker.stop_work_force();
		delayRefreshChoicesWorker.stop_work_force();
		votingPackagesWorker->stop_work();
		voteStatusWorker->stop_work();
	}
}
