#ifndef VOTER_APPLICATIONWINDOWCONTROLLER_H
#define VOTER_APPLICATIONWINDOWCONTROLLER_H


#include <mk-tfhe-voting/helpers/ICoordinatorForVoter.h>

#include <voter/Voter.h>
#include <voter/rest/VoterClient.h>

#include "votestatuscontroller.h"
#include "voteassistantcontroller.h"
#include "../view/votestatuswindow.h"
#include "../view/voteassistantwindow.h"
#include "../view/applicationwindow.h"
#include "../helpers/genericworker.h"
#include "../helpers/delay.h"

#include <vector>
#include <map>
#include <utility>


namespace hsr::applicationcontroller {
	class ApplicationController {

		hsr::applicationwindow::ApplicationWindow *applicationWindow{};

		std::shared_ptr<hsr::voter::Voter> voter;

		std::string coordinatorUri;

		hsr::voteassistantwindow::VoteAssistantWindow *voteAssistantWindow;

		std::shared_ptr<hsr::voteassistantcontroller::VoteAssistantController> voteAssistantController;

		hsr::votestatuswindow::VoteStatusWindow *voteStatusWindow;

		std::shared_ptr<hsr::voterstatuscontroller::VoteStatusController> voteStatusController;

		std::map<unsigned int, VotesText> voteTextListed;

		std::map<unsigned int, VotesText> voteTextChosenForVote;

		bool isCoordinatorNotReachableGetVotes{false};

		bool isCoordinatorNotReachableRegister{false};

		Glib::Dispatcher refreshDispatcher;

		std::shared_ptr<hsr::genericworker::GenericWorker<std::map<unsigned int, VotesText>>> refreshWorker;

		Glib::Dispatcher participationDispatcher;

		std::shared_ptr<hsr::genericworker::GenericWorker<std::vector<unsigned int>>> participationWorker;

		Glib::Dispatcher registerDispatcher;

		std::shared_ptr<hsr::genericworker::GenericWorker<bool>> registerWorker;

		Glib::Dispatcher votingPackagesDispatcher;

		std::shared_ptr<hsr::genericworker::GenericWorker<std::map<unsigned int, std::shared_ptr<VotingPackage>>>> votingPackagesWorker;

		Glib::Dispatcher voteStatusDispatcher;

		std::shared_ptr<hsr::genericworker::GenericWorker<std::map<unsigned, VoteStatusInformation>>> voteStatusWorker;

		Glib::Dispatcher delayRefreshButtonDispatcher;

		hsr::delay::Delay delayRefreshButtonWorker;

		Glib::Dispatcher delayRefreshChoicesDispatcher;

		hsr::delay::Delay delayRefreshChoicesWorker;

		std::vector<unsigned int> voteIds{};

		std::vector<unsigned int> participationIds;

		std::vector<sigc::connection> disposableSignals;

		int isElectionChecked() const;

		void update_widgets();

		void on_notification_from_worker_thread_refresh();

		void on_notification_from_worker_thread_register();

		void on_notification_from_worker_thread_participation();

		void on_notification_from_worker_thread_voteStatus();

		void on_notification_from_worker_thread_delay_refresh();

		void on_notification_from_worker_thread_delay_choices();

		void on_force_stop_request_get_Votes();

	public:
		explicit ApplicationController(hsr::applicationwindow::ApplicationWindow *applicationWindow, std::shared_ptr<hsr::voter::Voter> voter, std::string coordinatorUri);

		~ApplicationController();

		void updateRefreshButton();

		void onCheckButtonToggled();
	};
}


#endif //VOTER_APPLICATIONWINDOWCONTROLLER_H
