#include "votestatuscontroller.h"
#include "../view/votestatuswindow.h"

#include <utility>


namespace hsr::voterstatuscontroller {

	VoteStatusController::VoteStatusController(votestatuswindow::VoteStatusWindow *voteStatusWindow, std::map<unsigned, VoteStatusInformation> voterStatus)
			: voteStatusWindow{voteStatusWindow},
			  voteStatusInformation{std::move(voterStatus)} {}

	void VoteStatusController::showVoteStatusInformation(const std::vector<unsigned int> &participationIds) {

		int bbSize = voteStatusInformation.at(participationIds.front()).bb.size();
		if(bbSize == 0) {
			bbSize = 1;
		}
		voteStatusWindow->getQuestionGrid()->attach(*voteStatusWindow->getTitleAppLabel(), 1, 1, bbSize, 1);

		unsigned offset{};
		for (unsigned id : participationIds) {
			unsigned voteOffset{offset*6};
			auto &statusInfo{voteStatusInformation.at(id)};
			auto voterStatus{voteStatusWindow->getVoterLabel()};

			voterStatus->emplace_back(Gtk::Label("Vote " + std::to_string(id)));
			voterStatus->back().get_style_context()->add_class("big_bold_label");

			voterStatus->emplace_back(Gtk::Label("Voting Package received"));
			voterStatus->back().get_style_context()->add_class("bold_label");
			if (statusInfo.voter.status.at("packageReceived")) {
				voterStatus->emplace_back(Gtk::Label("Done"));
			} else {
				voterStatus->emplace_back(Gtk::Label("not yet"));
			}

			voterStatus->emplace_back(Gtk::Label("Vote sent"));
			voterStatus->back().get_style_context()->add_class("bold_label");
			if (statusInfo.voter.status.at("packageSent")) {
				voterStatus->emplace_back(Gtk::Label("Done"));
			} else {
				voterStatus->emplace_back(Gtk::Label("not yet"));
			}

			voteStatusWindow->getQuestionGrid()->attach(voterStatus->at(voterStatus->size() - 5), 1, voteOffset+2, bbSize, 1);
			voteStatusWindow->getQuestionGrid()->attach(voterStatus->at(voterStatus->size() - 4), 1, voteOffset+3, bbSize, 1);
			voteStatusWindow->getQuestionGrid()->attach(voterStatus->at(voterStatus->size() - 3), 1, voteOffset+4, bbSize, 1);
			voteStatusWindow->getQuestionGrid()->attach(voterStatus->at(voterStatus->size() - 2), 1, voteOffset+5, bbSize, 1);
			voteStatusWindow->getQuestionGrid()->attach(voterStatus->at(voterStatus->size() - 1), 1, voteOffset+6, bbSize, 1);

			int bbCounter{1};
			for (auto &bb : statusInfo.bb) {
				int bbNumber{bbCounter};
				auto bulletinboardStatus{voteStatusWindow->getBbLabel()};


				bulletinboardStatus->emplace_back(Gtk::Label("Bulletin Board " + std::to_string(bb.first)));
				bulletinboardStatus->back().get_style_context()->add_class("big_bold_label");

				bulletinboardStatus->emplace_back(Gtk::Label("Vote Received"));
				bulletinboardStatus->back().get_style_context()->add_class("bold_label");
				if (bb.second.status.at("voteReceived")) {
					bulletinboardStatus->emplace_back(Gtk::Label("Done"));
				} else {
					bulletinboardStatus->emplace_back(Gtk::Label("not yet"));
				}
				voteStatusWindow->getBbBox()->emplace_back(Gtk::Box(Gtk::ORIENTATION_VERTICAL));
				voteStatusWindow->getBbBox()->back().add(bulletinboardStatus->at(voteStatusWindow->getBbLabel()->size() - 3));
				voteStatusWindow->getBbBox()->back().add(bulletinboardStatus->at(voteStatusWindow->getBbLabel()->size() - 2));
				voteStatusWindow->getBbBox()->back().add(bulletinboardStatus->at(voteStatusWindow->getBbLabel()->size() - 1));



				bulletinboardStatus->emplace_back(Gtk::Label("Vote Dequeued"));
				bulletinboardStatus->back().get_style_context()->add_class("bold_label");
				if (bb.second.status.at("voteDequeued")) {
					bulletinboardStatus->emplace_back(Gtk::Label("Done"));
				} else {
					bulletinboardStatus->emplace_back(Gtk::Label("not yet"));
				}
				voteStatusWindow->getBbBox()->emplace_back(Gtk::Box(Gtk::ORIENTATION_VERTICAL));
				voteStatusWindow->getBbBox()->back().add(bulletinboardStatus->at(voteStatusWindow->getBbLabel()->size() - 2));
				voteStatusWindow->getBbBox()->back().add(bulletinboardStatus->at(voteStatusWindow->getBbLabel()->size() - 1));


				bulletinboardStatus->emplace_back(Gtk::Label("Vote Processed"));
				bulletinboardStatus->back().get_style_context()->add_class("bold_label");
				if (bb.second.status.at("voteProcessed")) {
					bulletinboardStatus->emplace_back(Gtk::Label("Done"));
				} else {
					bulletinboardStatus->emplace_back(Gtk::Label("not yet"));
				}
				voteStatusWindow->getBbBox()->emplace_back(Gtk::Box(Gtk::ORIENTATION_VERTICAL));
				voteStatusWindow->getBbBox()->back().add(bulletinboardStatus->at(voteStatusWindow->getBbLabel()->size() - 2));
				voteStatusWindow->getBbBox()->back().add(bulletinboardStatus->at(voteStatusWindow->getBbLabel()->size() - 1));


				voteStatusWindow->getBbGrid()->emplace_back(Gtk::Grid());
				voteStatusWindow->getBbGrid()->back().attach(
						voteStatusWindow->getBbBox()->at(voteStatusWindow->getBbBox()->size() - 3), bbNumber, 1);
				voteStatusWindow->getBbGrid()->back().get_style_context()->add_class("status_bb_grid");

				voteStatusWindow->getBbGrid()->back().attach(
						voteStatusWindow->getBbBox()->at(voteStatusWindow->getBbBox()->size() - 2), bbNumber, 2);
				voteStatusWindow->getBbGrid()->back().get_style_context()->add_class("status_bb_grid");

				voteStatusWindow->getBbGrid()->back().attach(
						voteStatusWindow->getBbBox()->at(voteStatusWindow->getBbBox()->size() - 1), bbNumber, 3);
				voteStatusWindow->getBbGrid()->back().get_style_context()->add_class("status_bb_grid");

				voteStatusWindow->getQuestionGrid()->attach(voteStatusWindow->getBbGrid()->back(), bbNumber, voteOffset+7);
				++bbCounter;
			}

			++offset;
		}
		voteStatusWindow->getMainGrid()->attach(*voteStatusWindow->getQuestionGrid(), 1, 1);

		voteStatusWindow->add(*voteStatusWindow->getMainGrid());

		voteStatusWindow->show_all_children();
	}

	VoteStatusController::~VoteStatusController() {
		delete voteStatusWindow;
	}
}
