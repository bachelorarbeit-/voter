#ifndef MK_TFHE_VOTING_VOTESTATUSCONTROLLER_H
#define MK_TFHE_VOTING_VOTESTATUSCONTROLLER_H


#include <voter/Voter.h>

#include <mk-tfhe-voting/helpers/votetrackinginformation.h>
#include <voter/rest/VoterClient.h>
#include "../view/votestatuswindow.h"

#include <memory>
#include <map>


namespace hsr::voterstatuscontroller {
	class VoteStatusController {

		votestatuswindow::VoteStatusWindow *voteStatusWindow;

		std::vector<unsigned int> voteIds;

		std::map<unsigned int, VoteStatusInformation> voteStatusInformation;

	public:
		VoteStatusController(votestatuswindow::VoteStatusWindow *voteStatusWindow, std::map<unsigned int, VoteStatusInformation> voterStatus);

		void showVoteStatusInformation(const std::vector<unsigned int> &participationIds);

		virtual ~VoteStatusController();
	};
}


#endif //MK_TFHE_VOTING_VOTESTATUSCONTROLLER_H
