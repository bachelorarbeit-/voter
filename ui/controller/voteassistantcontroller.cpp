#include "voteassistantcontroller.h"
#include <voter/rest/VoterClient.h>

#include <mk-tfhe-voting/exceptions/BulletinBoardError.h>

#include <iostream>
#include <utility>

#include <cpprest/http_msg.h>


namespace hsr::voteassistantcontroller {
	VoteAssistantController::VoteAssistantController(hsr::voteassistantwindow::VoteAssistantWindow *voteAssistantWindow, std::shared_ptr<hsr::voter::Voter> voter,
													 std::map<unsigned int, VotesText> &votingText,
													 std::shared_ptr<hsr::genericworker::GenericWorker<std::map<unsigned int, std::shared_ptr<VotingPackage>>>> &votingPackageWorker,
													 std::function<void()> refreshVotesFromAssistant) :
			voteAssistantWindow{voteAssistantWindow},
			voter{std::move(voter)},
			votingText{votingText},
			votingPackageWorker{votingPackageWorker},
			refreshVotesFromAssistant{std::move(refreshVotesFromAssistant)},
			voteSendDispatcher{},
			voteTrackPackageDispatcher{},
			voteTrackSendDispatcher{} {

		voteSendWorker = std::make_shared<hsr::genericworker::GenericWorker<bool>>(voteSendDispatcher, [this] {
			try {
				this->voter->vote(this->elections, this->getSelectedChoices());
				return true;
			} catch (web::http::http_exception const &e) {
				std::cout << "Bulletin boards are not reachable to send vote.\n" << e.what() << "\n\n";
			} catch (BulletinBoardError const &e) {
				std::cout << e.what() << "\n\n";
			}
			return false;
		});

		voteTrackPackageWorker = std::make_shared<hsr::genericworker::GenericWorker<bool>>(voteTrackPackageDispatcher, [this] {
			try {
				this->voter->getCoordinator().lock()->trackVoteReceivedPackage(this->extractBallotIds());
				return true;
			} catch (web::http::http_exception const &e) {
				std::cout << "Couldn't send 'packageReceived' status to coordinator.\n" << e.what() << "\n\n";
			}
			return false;
		});

		voteTrackSendWorker = std::make_shared<hsr::genericworker::GenericWorker<bool>>(voteTrackSendDispatcher, [this] {
			try {
				this->voter->getCoordinator().lock()->trackVoteSentVote(this->extractBallotIds());
				return true;
			} catch (web::http::http_exception const &e) {
				std::cout << "Couldn't send 'voteSent' status to coordinator.\n" << e.what() << "\n\n";
			}
			return false;
		});

		usedSignals.emplace_back(voteSendDispatcher.connect(sigc::mem_fun(*this, &VoteAssistantController::on_notification_from_worker_thread_voteSent)));

		initVoteAssistantWindow();
	}

	void VoteAssistantController::initVoteAssistantWindow() {
		unsigned int pageNr{0};

		initOverview(pageNr);
		initVotes(pageNr);
		initConfirmation(pageNr);
		initEnd(pageNr);

		voteAssistantWindow->signal_apply().connect(sigc::mem_fun(*this, &VoteAssistantController::on_assistant_apply));
		voteAssistantWindow->signal_cancel().connect(sigc::mem_fun(*this, &VoteAssistantController::on_assistant_cancel));
		voteAssistantWindow->signal_close().connect(sigc::mem_fun(*this, &VoteAssistantController::on_assistant_close));
		voteAssistantWindow->signal_prepare().connect(sigc::mem_fun(*this, &VoteAssistantController::on_assistant_prepare));

		voteAssistantWindow->show_all_children();
	}

	void VoteAssistantController::initOverview(unsigned int &pageNr) {
		voteAssistantWindow->getStartEndPages()->emplace_back(Gtk::Box(Gtk::ORIENTATION_VERTICAL, 12));
		voteAssistantWindow->getStartEndPages()->back().pack_start(*voteAssistantWindow->getOverview());
		voteAssistantWindow->append_page(voteAssistantWindow->getStartEndPages()->back());
		voteAssistantWindow->set_page_type(voteAssistantWindow->getStartEndPages()->back(), Gtk::ASSISTANT_PAGE_INTRO);
		voteAssistantWindow->set_page_complete(voteAssistantWindow->getStartEndPages()->back(), true);
		voteAssistantWindow->set_page_title(*voteAssistantWindow->get_nth_page(static_cast<int>(pageNr++)), "Overview");
	}

	void VoteAssistantController::initVotes(unsigned int &pageNr) {
		for (const auto &vt : votingText) {
			generateVote(vt.second.uniqueIdentifier, vt.second.question, vt.second.choices);
		}

		voteAssistantWindow->getStartEndPages()->back().pack_start(*voteAssistantWindow->getVoteQuestionsTitle());
		for (auto &question : *voteAssistantWindow->getQuestionsOverview()) {
			voteAssistantWindow->getStartEndPages()->back().pack_end(question);
		}

		for (auto &vote : *voteAssistantWindow->getVotePages()) {
			voteAssistantWindow->append_page(vote.second);
			voteAssistantWindow->set_page_complete(vote.second, true);
			voteAssistantWindow->set_page_title(*voteAssistantWindow->get_nth_page(static_cast<int>(pageNr++)), "Vote " + std::to_string(vote.first));
			voteAssistantWindow->set_page_type(voteAssistantWindow->getStartEndPages()->back(), Gtk::ASSISTANT_PAGE_CONTENT);
		}
	}

	void VoteAssistantController::initConfirmation(unsigned int &pageNr) {
		voteAssistantWindow->getStartEndPages()->emplace_back(Gtk::Box(Gtk::ORIENTATION_VERTICAL, 12));
		voteAssistantWindow->getStartEndPages()->back().pack_start(*voteAssistantWindow->getConfirmation());
		voteAssistantWindow->append_page(voteAssistantWindow->getStartEndPages()->back());
		this->confirmationPageIndex = voteAssistantWindow->getStartEndPages()->size() - 1;
		voteAssistantWindow->getStartEndPages()->back().set_name(this->confirmationPageName);
		voteAssistantWindow->set_page_title(*voteAssistantWindow->get_nth_page(static_cast<int>(pageNr++)), this->confirmationPageName);
		voteAssistantWindow->set_page_type(voteAssistantWindow->getStartEndPages()->back(), Gtk::ASSISTANT_PAGE_CONFIRM);
		voteAssistantWindow->getStartEndPages()->back().pack_end(*voteAssistantWindow->getConfirmationSummaryBox());
		voteAssistantWindow->getConfirmationSummaryBox()->pack_start(*voteAssistantWindow->getConfirmationSummaryTitle());
		voteAssistantWindow->getStartEndPages()->back().pack_end(*voteAssistantWindow->getSpinVotingPackage());
		voteAssistantWindow->getStartEndPages()->back().pack_end(*voteAssistantWindow->getConfirmationSpinnerText());
		voteAssistantWindow->getSpinVotingPackage()->start();
	}

	void VoteAssistantController::initEnd(unsigned int &pageNr) {
		voteAssistantWindow->getStartEndPages()->emplace_back(Gtk::Box(Gtk::ORIENTATION_VERTICAL, 12));
		voteAssistantWindow->getStartEndPages()->back().pack_start(*voteAssistantWindow->getEndPage());
		voteAssistantWindow->append_page(voteAssistantWindow->getStartEndPages()->back());
		this->endPageIndex = voteAssistantWindow->getStartEndPages()->size() - 1;
		voteAssistantWindow->set_page_title(*voteAssistantWindow->get_nth_page(static_cast<int>(pageNr)), "End");
		voteAssistantWindow->set_page_type(voteAssistantWindow->getStartEndPages()->back(), Gtk::ASSISTANT_PAGE_SUMMARY);
		voteAssistantWindow->set_page_complete(voteAssistantWindow->getStartEndPages()->at(this->endPageIndex), false);
		voteAssistantWindow->getStartEndPages()->back().pack_end(*voteAssistantWindow->getEndPageText());
		voteAssistantWindow->getEndPageText()->hide();
		voteAssistantWindow->getStartEndPages()->back().pack_end(*voteAssistantWindow->getSpinSubmitVotes());
		voteAssistantWindow->getStartEndPages()->back().pack_end(*voteAssistantWindow->getEndPageSpinnerText());
		voteAssistantWindow->getSpinSubmitVotes()->start();
	}

	void VoteAssistantController::on_assistant_apply() {
		voteSendWorker->run();
		votingText.clear();
	}

	void VoteAssistantController::on_assistant_cancel() {
		refreshVotesFromAssistant();
		voteAssistantWindow->hide();
	}

	void VoteAssistantController::on_assistant_close() {
		voteAssistantWindow->hide();
	}

	void VoteAssistantController::on_assistant_prepare(Gtk::Widget *page) {
		voteAssistantWindow->set_title(Glib::ustring::compose("gtk-test (Page %1 of %2)", voteAssistantWindow->get_current_page() + 1, voteAssistantWindow->get_n_pages()));
		if (page->get_name() == this->confirmationPageName) {
			voteAssistantWindow->getConfirmationSummary()->clear();
			for (auto &vote : *voteAssistantWindow->getVoteOptions()) {
				auto questionItr = votingText.begin();
				for (auto &option : vote) {
					if (option.second.get_active()) {
						voteAssistantWindow->getConfirmationSummary()->emplace_back(Gtk::Label(questionItr->second.question));
						voteAssistantWindow->getConfirmationSummaryBox()->pack_start(voteAssistantWindow->getConfirmationSummary()->back());

						voteAssistantWindow->getConfirmationSummary()->emplace_back(Gtk::Label(Gtk::Label("\u2022 " + option.second.get_label())));
						voteAssistantWindow->getConfirmationSummaryBox()->pack_start(voteAssistantWindow->getConfirmationSummary()->back());

						++questionItr;
					}
				}
			}
			voteAssistantWindow->getConfirmationSummaryBox()->show_all_children();
		}
	}

	void VoteAssistantController::printStatus() {
		std::cout << "Results:\n";
		for (auto &vote : *voteAssistantWindow->getVoteOptions()) {
			for (auto &option : vote) {
				if (option.second.get_active()) {
					std::cout << option.second.get_label() << "\n";
				}
			}
		}
		std::cout << std::endl;
	}

	void VoteAssistantController::generateVote(unsigned int uniqueIdentifier, std::string const &question, std::vector<std::string> const &options) {
		voteAssistantWindow->getVotePages()->insert({uniqueIdentifier, Gtk::Box(Gtk::ORIENTATION_VERTICAL, 12)});
		voteAssistantWindow->getVoteOptions()->emplace_back();

		voteAssistantWindow->getQuestions()->emplace_back(question);
		voteAssistantWindow->getQuestions()->back().get_style_context()->add_class("assistant_title");
		voteAssistantWindow->getQuestionsOverview()->emplace_back("\u2022 " + question);
		voteAssistantWindow->getQuestionsOverview()->back().get_style_context()->add_class("assistant_question_overview");
		voteAssistantWindow->getVotePages()->at(uniqueIdentifier).pack_start(voteAssistantWindow->getQuestions()->back());

		std::size_t optionStartNumber{1};

		for (std::size_t i{optionStartNumber}; i <= options.size(); ++i) {
			voteAssistantWindow->getVoteOptions()->back().insert({i, Gtk::RadioButton(options.at(i - 1))});
			voteAssistantWindow->getVoteOptions()->back().at(i).join_group(voteAssistantWindow->getVoteOptions()->back().begin()->second);
			voteAssistantWindow->getVotePages()->at(uniqueIdentifier).pack_start(voteAssistantWindow->getVoteOptions()->back().at(i));
		}

		elections.emplace_back(uniqueIdentifier);
	}

	std::vector<unsigned int> VoteAssistantController::getSelectedChoices() {
		std::vector<unsigned int> choices;

		for (auto &vote : *voteAssistantWindow->getVoteOptions()) {
			for (auto &option : vote) {
				if (option.second.get_active()) {
					choices.emplace_back(option.first - 1);
				}
			}
		}

		return choices;
	}

	std::vector<unsigned int> VoteAssistantController::extractBallotIds() {
		std::vector<unsigned int> ballotIds{};
		for (auto &vp : votingPackage) {
			ballotIds.emplace_back(vp.second->ballotId);
		}
		return ballotIds;
	}

	void VoteAssistantController::disconnectUsedSignals() {
		for (auto &sig : usedSignals) {
			sig.disconnect();
		}
	}

	VoteAssistantController::~VoteAssistantController() {
		votingPackageWorker->stop_work();
		voteSendWorker->stop_work();
		voteTrackPackageWorker->stop_work();
		voteTrackSendWorker->stop_work();
		disconnectUsedSignals();
		delete voteAssistantWindow;
	}

	void VoteAssistantController::on_notification_from_worker_thread_votingPackage() {
		voteAssistantWindow->set_page_complete(voteAssistantWindow->getStartEndPages()->at(this->confirmationPageIndex), true);
		voteAssistantWindow->getConfirmationSpinnerText()->hide();
		voteAssistantWindow->getSpinVotingPackage()->stop();
		try {
			votingPackage = votingPackageWorker->get_data();
		} catch (web::http::http_exception const &e) {
			std::cout << "Couldn't get voting packages";
		}

		voteTrackPackageWorker->run();
		usedSignals.emplace_back(voteTrackPackageDispatcher.connect(sigc::mem_fun(*this, &VoteAssistantController::on_notification_from_worker_thread_votePackageReceivedTracked)));
	}

	void VoteAssistantController::on_notification_from_worker_thread_voteSent() {
		try {
			voteSendWorker->get_data();
		} catch (web::http::http_exception const &e) {
			std::cout << "Couldn't send votes to bulletin boards.";
		}

		voteAssistantWindow->set_page_complete(voteAssistantWindow->getStartEndPages()->at(this->endPageIndex), true);
		voteAssistantWindow->getEndPageSpinnerText()->hide();
		voteAssistantWindow->getSpinSubmitVotes()->stop();

		voteTrackSendWorker->run();
		usedSignals.emplace_back(voteTrackSendDispatcher.connect(sigc::mem_fun(*this, &VoteAssistantController::on_notification_from_worker_thread_voteSentTracked)));
		refreshVotesFromAssistant();
	}

	void VoteAssistantController::on_notification_from_worker_thread_votePackageReceivedTracked() {
		try {
			voteTrackPackageWorker->get_data();
		} catch (web::http::http_exception const &e) {
			std::cout << "Couldn't track vote packages to coordinator.";
		}
	}

	void VoteAssistantController::on_notification_from_worker_thread_voteSentTracked() {
		try {
			voteTrackSendWorker->get_data();
			voteAssistantWindow->getEndPageText()->show();
		} catch (web::http::http_exception const &e) {
			std::cout << "Couldn't track sent votes to coordinator.";
		}
	}
}
