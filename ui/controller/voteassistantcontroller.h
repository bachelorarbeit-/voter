#ifndef MK_TFHE_VOTING_VOTEASSISTANTCONTROLLER_H
#define MK_TFHE_VOTING_VOTEASSISTANTCONTROLLER_H


#include <voter/Voter.h>

#include "../view/voteassistantwindow.h"

#include <vector>
#include <map>
#include <functional>


namespace hsr::voteassistantcontroller {
	class VoteAssistantController {

		voteassistantwindow::VoteAssistantWindow *voteAssistantWindow;

		std::shared_ptr<hsr::voter::Voter> voter;

		std::map<unsigned int, VotesText> &votingText;

		std::map<unsigned int, std::shared_ptr<VotingPackage>> votingPackage;

		std::shared_ptr<hsr::genericworker::GenericWorker<std::map<unsigned int, std::shared_ptr<VotingPackage>>>> &votingPackageWorker;

		std::function<void()> refreshVotesFromAssistant;

		std::vector<unsigned int> elections;

		std::size_t confirmationPageIndex{};

		std::string confirmationPageName{"Confirmation"};

		std::size_t endPageIndex{};

		std::vector<sigc::connection> usedSignals;

		Glib::Dispatcher voteSendDispatcher;
		std::shared_ptr<hsr::genericworker::GenericWorker<bool>> voteSendWorker;

		Glib::Dispatcher voteTrackPackageDispatcher;
		std::shared_ptr<hsr::genericworker::GenericWorker<bool>> voteTrackPackageWorker;

		Glib::Dispatcher voteTrackSendDispatcher;
		std::shared_ptr<hsr::genericworker::GenericWorker<bool>> voteTrackSendWorker;

		void initVoteAssistantWindow();

		void initOverview(unsigned int &pageNr);

		void initVotes(unsigned int &pageNr);

		void initConfirmation(unsigned int &pageNr);

		void initEnd(unsigned int &pageNr);

		void on_assistant_apply();

		void on_assistant_cancel();

		void on_assistant_close();

		void on_assistant_prepare(Gtk::Widget *widget);

		void printStatus();

		void generateVote(unsigned int uniqueIdentifier, std::string const &question, std::vector<std::string> const &options);

		std::vector<unsigned int> getSelectedChoices();

		std::vector<unsigned int> extractBallotIds();

		void disconnectUsedSignals();

	public:
		VoteAssistantController(hsr::voteassistantwindow::VoteAssistantWindow *voteAssistantWindow,
								std::shared_ptr<hsr::voter::Voter> voter,
								std::map<unsigned int, VotesText> &votingText,
								std::shared_ptr<hsr::genericworker::GenericWorker<std::map<unsigned int,
										std::shared_ptr<VotingPackage>>>> &votingPackageWorker,
								std::function<void()> refreshVotesFromAssistant);

		virtual ~VoteAssistantController();

		void on_notification_from_worker_thread_votingPackage();

		void on_notification_from_worker_thread_voteSent();

		void on_notification_from_worker_thread_votePackageReceivedTracked();

		void on_notification_from_worker_thread_voteSentTracked();
	};
}


#endif //MK_TFHE_VOTING_VOTEASSISTANTCONTROLLER_H
