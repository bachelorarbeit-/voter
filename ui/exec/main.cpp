#include "voterapplication.h"


int main(int argc, char *argv[]) {
	auto application = hsr::voteapplication::VoterApplication::create();
	return application->run(argc, argv);
}