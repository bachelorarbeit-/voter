#include "voterapplication.h"

#include <iostream>


namespace hsr::voteapplication {
	VoterApplication::VoterApplication() : Gtk::Application("ch.hsr.voter", Gio::APPLICATION_HANDLES_OPEN) {}

	Glib::RefPtr<VoterApplication> VoterApplication::create() {
		return Glib::RefPtr<VoterApplication>(new VoterApplication());
	}

	hsr::applicationwindow::ApplicationWindow *VoterApplication::create_app_window() {
		std::string coordinatorUri{"http://localhost:8080"};
		this->voteClient = std::make_shared<hsr::voteclient::VoterClient>(coordinatorUri);
		this->voter = std::make_shared<hsr::voter::Voter>(std::weak_ptr<hsr::voteclient::VoterClient>{voteClient});
		auto applicationWindow = new hsr::applicationwindow::ApplicationWindow();
		this->applicationWindowController = std::make_shared<hsr::applicationcontroller::ApplicationController>(applicationWindow, voter, coordinatorUri);

		loadStyleFromCss();

		add_window(*applicationWindow);

		applicationWindow->signal_hide().connect(sigc::bind<Gtk::Window *>(sigc::mem_fun(*this, &VoterApplication::on_hide_window), applicationWindow));

		return applicationWindow;
	}

	void VoterApplication::loadStyleFromCss() const {
		Glib::RefPtr<Gtk::CssProvider> cssProvider = Gtk::CssProvider::create();
		std::string cssFile{"./style.css"};
		try {
			cssProvider->load_from_path(cssFile);
		} catch (Glib::Exception const &e) {
			std::cout << "Couldn't read " << cssFile << " file.\n" << e.what() << "\n\n";
		}

		Glib::RefPtr<Gtk::StyleContext> styleContext = Gtk::StyleContext::create();

		Glib::RefPtr<Gdk::Screen> screen = Gdk::Screen::get_default();

		styleContext->add_provider_for_screen(screen, cssProvider, GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
	}

	void VoterApplication::on_startup() {
		Application::on_startup();
	}

	void VoterApplication::on_activate() {
		auto applicationWindow = create_app_window();
		applicationWindow->present();
	}

	void VoterApplication::on_hide_window(Gtk::Window *window) {
		delete window;
	}
}