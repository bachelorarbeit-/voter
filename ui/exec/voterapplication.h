#ifndef VOTER_VOTERAPPLICATION_H
#define VOTER_VOTERAPPLICATION_H

#include <gtkmm.h>

#include <memory>

#include <voter/rest/VoterClient.h>
#include <voter/Voter.h>

#include "../view/applicationwindow.h"
#include "../controller/applicationcontroller.h"


namespace hsr::voteapplication {
	class VoterApplication : public Gtk::Application {

		hsr::applicationwindow::ApplicationWindow *create_app_window();

		void loadStyleFromCss() const;

		void on_hide_window(Gtk::Window *window);

		std::shared_ptr<hsr::applicationcontroller::ApplicationController> applicationWindowController;

		std::shared_ptr<hsr::voteclient::VoterClient> voteClient;

		std::shared_ptr<hsr::voter::Voter> voter;

	protected:
		VoterApplication();

		void on_startup() override;

		void on_activate() override;

	public:
		static Glib::RefPtr<VoterApplication> create();
	};
}


#endif //VOTER_VOTERAPPLICATION_H
