#ifndef VOTER_REFRESHWORKER_H
#define VOTER_REFRESHWORKER_H


#include <voter/exceptions/GuiWorkerError.h>

#include <gtkmm.h>

#include <thread>
#include <mutex>
#include <functional>
#include <optional>
#include <stdexcept>


namespace hsr::genericworker {
	template<typename T>
	class GenericWorker {

		Glib::Dispatcher &dispatcher;

		std::function<T()> task;

		mutable std::mutex mutex;

		std::thread workerThread;

		bool hasStopped;

		std::optional<T> data;

	public:
		GenericWorker(Glib::Dispatcher &dispatcher, std::function<T()> task) :
				dispatcher{dispatcher},
				task{task},
				mutex(),
				hasStopped(true) {
		}

		T get_data() {
			if (data) {
				if (this->workerThread.joinable()) {
					this->workerThread.join();
				}
				return data.value();
			}
			throw GuiWorkerError{"Value id not available"};
		}

		void stop_work() {
			std::lock_guard<std::mutex> lock(mutex);
			if (this->workerThread.joinable()) {
				this->workerThread.join();
			}
		}

		bool stopped() const {
			std::lock_guard<std::mutex> lock(mutex);
			return hasStopped;
		}

		void run() {
			std::lock_guard<std::mutex> lockStopped(mutex);
			if (hasStopped) {
				this->workerThread = std::thread{[this] {
					{
						std::lock_guard<std::mutex> lock(mutex);
						hasStopped = false;
					}
					this->data = task();
					{
						std::lock_guard<std::mutex> lock(mutex);
						hasStopped = true;
					}

					dispatcher.emit();
				}};
			}
		}

	};
}


#endif //VOTER_REFRESHWORKER_H
