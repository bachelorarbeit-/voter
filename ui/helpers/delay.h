#ifndef VOTER_DELAY_H
#define VOTER_DELAY_H


#include <voter/exceptions/GuiWorkerError.h>

#include <gtkmm.h>

#include <thread>
#include <mutex>
#include <functional>
#include <optional>
#include <stdexcept>


namespace hsr::delay {
	class Delay {

		Glib::Dispatcher &dispatcher;

		unsigned int delayTime;

		mutable std::mutex mutex;

		std::thread workerThread;

		bool hasStopped;

	public:
		Delay(Glib::Dispatcher &dispatcher, unsigned int delayTime) :
				dispatcher{dispatcher},
				delayTime{delayTime},
				mutex(),
				hasStopped(true) {
		}

		void stop_work() {
			std::lock_guard<std::mutex> lock(mutex);
			if (this->workerThread.joinable()) {
				this->workerThread.join();
			} else {
				throw GuiWorkerError{"The Delay hasn't been reached yet."};
			}
		}

		void stop_work_force() {
			if (this->workerThread.joinable()) {
				this->workerThread.join();
			}
		}

		bool stopped() const {
			std::lock_guard<std::mutex> lock(mutex);
			return hasStopped;
		}

		void run() {
			std::lock_guard<std::mutex> lockStopped(mutex);
			if (hasStopped) {
				this->workerThread = std::thread{[this] {
					{
						std::lock_guard<std::mutex> lock(mutex);
						hasStopped = false;
					}

					std::this_thread::sleep_for(std::chrono::seconds(delayTime));

					{
						std::lock_guard<std::mutex> lock(mutex);
						hasStopped = true;
					}

					dispatcher.emit();
				}};
			}
		}
	};
}


#endif //VOTER_DELAY_H
