#include "applicationwindow.h"

#include <gtkmm.h>


namespace hsr::applicationwindow {
	ApplicationWindow::ApplicationWindow()
			: Gtk::ApplicationWindow(),
			  m_titleAppLabel("E-Voting System MK-TFHE", false),
			  m_coordinatorUri("TEST", false),
			  m_titleElectionsLabel("Available Elections", false),
			  m_questionBox(Gtk::ORIENTATION_VERTICAL),
			  m_startVoteButtonBox(Gtk::ORIENTATION_VERTICAL),
			  m_startVoteButton("Start Vote", false),
			  m_refreshBox(Gtk::ORIENTATION_VERTICAL),
			  m_refreshVotesButton("Refresh", false),
			  m_refreshTitleLabel("Last Refresh", false),
			  m_refreshTimeLabel("<Not refreshed yet>", false),
			  m_statusVotesButtonBox(Gtk::ORIENTATION_VERTICAL),
			  m_statusVotesButton("Status Votes", false),
			  m_buttonQuit("Quit") {

		set_title("E-Voting System MK TFHE");
		set_default_size(window_width, -1);
		m_titleAppLabel.set_size_request(window_width, -1);
		m_questionBox.set_size_request(-1, 300);

		m_titleAppLabel.get_style_context()->add_class("window_title");
		m_titleElectionsLabel.set_name("title_elections_label");
		m_coordinatorUri.set_name("coordinator_uri");
		m_questionBox.set_name("question_box");
		m_refreshTitleLabel.get_style_context()->add_class("bold_label");

		m_startVoteButton.set_sensitive(false);

		add(m_mainGrid);

		m_buttonQuit.set_can_default();
		m_buttonQuit.grab_default();
		m_buttonQuit.signal_clicked().connect(sigc::mem_fun(*this, &ApplicationWindow::on_button_quit));
		m_buttonQuit.set_border_width(10);

		m_refreshVotesButton.signal_clicked().connect(sigc::mem_fun(this, &ApplicationWindow::onRefreshCallback));
		m_startVoteButton.signal_clicked().connect(sigc::mem_fun(this, &ApplicationWindow::onStartCallback));
		m_statusVotesButton.signal_clicked().connect(sigc::mem_fun(this, &ApplicationWindow::onStatusCallback));

		m_mainGrid.attach(m_titleAppLabel, 1, 1, 2, 1);
		m_mainGrid.attach(m_coordinatorUri, 1, 2, 2, 1);

		m_mainGrid.attach(m_titleElectionsLabel, 1, 3, 1, 1);
		m_mainGrid.attach(m_questionBox, 1, 4, 1, 3);

		m_startVoteButtonBox.add(m_startVoteButtonServerStatus);
		m_startVoteButtonBox.add(m_startVoteButton);
		m_mainGrid.attach(m_startVoteButtonBox, 1, 7, 1, 1);

		m_refreshBox.add(m_refreshVotesButton);
		m_refreshBox.add(m_refreshTitleLabel);
		m_refreshBox.add(m_refreshTimeLabel);
		m_mainGrid.attach(m_refreshBox, 2, 3, 1, 1);

		m_statusVotesButtonBox.add(m_statusVotesButtonServerStatus);
		m_statusVotesButtonBox.add(m_statusVotesButton);
		m_mainGrid.attach(m_statusVotesButtonBox, 2, 7, 1, 1);

		m_startVoteButtonServerStatus.get_style_context()->add_class("important_label");
		m_statusVotesButtonServerStatus.get_style_context()->add_class("important_label");

		show_all_children();
	}

	void ApplicationWindow::on_button_quit() {
		hide();
	}

	void ApplicationWindow::setOnStartVote(const std::function<void()> &onStartVote) {
		this->startVote = onStartVote;
	}

	void ApplicationWindow::setOnStatusVotes(const std::function<void()> &onStatusVote) {
		this->statusVote = onStatusVote;
	}

	void ApplicationWindow::setOnRefreshVote(const std::function<void()> &onRefresh) {
		this->refresh = onRefresh;
	}

	void ApplicationWindow::onStartCallback() {
		if (this->startVote) {
			this->startVote.value()();
		}
	}

	void ApplicationWindow::onStatusCallback() {
		if (this->statusVote) {
			this->statusVote.value()();
		}
	}

	void ApplicationWindow::onRefreshCallback() {
		if (this->refresh) {
			this->refresh.value()();
		}
	}

	Gtk::Grid *ApplicationWindow::getMainGrid() {
		return &m_mainGrid;
	}

	Gtk::Label *ApplicationWindow::getCoordinatorUri() {
		return &m_coordinatorUri;
	}

	Gtk::Button *ApplicationWindow::getStartVoteButton() {
		return &m_startVoteButton;
	}

	Gtk::Label *ApplicationWindow::getRefreshTimeLabel() {
		return &m_refreshTimeLabel;
	}

	Gtk::Button *ApplicationWindow::getRefreshVotesButton() {
		return &m_refreshVotesButton;
	}

	Gtk::Box *ApplicationWindow::getQuestionBox() {
		return &m_questionBox;
	}

	std::map<unsigned int, Gtk::CheckButton> *ApplicationWindow::getQuestions() {
		return &m_questions;
	}

	Gtk::Label *ApplicationWindow::getStartVoteButtonServerStatus() {
		return &m_startVoteButtonServerStatus;
	}

	Gtk::Label *ApplicationWindow::getStatusVotesButtonServerStatus() {
		return &m_statusVotesButtonServerStatus;
	}
}