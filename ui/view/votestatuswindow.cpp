#include "votestatuswindow.h"


namespace hsr::votestatuswindow {
	VoteStatusWindow::VoteStatusWindow() :
			Gtk::Window(),
			m_titleAppLabel("Vote Status") {

		m_titleAppLabel.set_size_request(window_width, -1);

		m_titleAppLabel.get_style_context()->add_class("window_title");
		m_main_grid.set_name("status_main_grid");
	}

	Gtk::Grid *VoteStatusWindow::getMainGrid() {
		return &m_main_grid;
	}

	Gtk::Label *VoteStatusWindow::getTitleAppLabel() {
		return &m_titleAppLabel;
	}

	Gtk::Grid *VoteStatusWindow::getQuestionGrid() {
		return &m_question_grid;
	}

	std::vector<Gtk::Label> *VoteStatusWindow::getVoterLabel() {
		return &m_voter_label;
	}

	std::vector<Gtk::Box> *VoteStatusWindow::getBbBox() {
		return &m_bb_box;
	}

	std::vector<Gtk::Label> *VoteStatusWindow::getBbLabel() {
		return &m_bb_label;
	}

	std::vector<Gtk::Grid> *VoteStatusWindow::getBbGrid() {
		return &m_bb_grid;
	}
}
