#include <voter/Voter.h>

#include "voteassistantwindow.h"
#include "../helpers/genericworker.h"


namespace hsr::voteassistantwindow {
	VoteAssistantWindow::VoteAssistantWindow() :
			m_overview("Overview"),
			m_voteQuestionsTitle("You have chosen the following questions:"),
			m_confirmation("Confirmation"),
			m_confirmationSpinnerText("Loading for voting package ..."),
			m_confirmationSummaryBox(Gtk::ORIENTATION_VERTICAL),
			m_confirmationSummaryTitle("Summary of the chosen choices:"),
			m_endPage("Goodbye"),
			m_endPageSpinnerText("Sending vote(s) ..."),
			m_endPageText("Thank you for participating in this election. Don't forget to vote the next time."),
			m_voteOptions{} {

		set_title("Vote Assistant");
		set_default_size(window_width, window_height);

		m_overview.get_style_context()->add_class("assistant_title");
		m_confirmation.get_style_context()->add_class("assistant_title");
		m_confirmationSummaryTitle.get_style_context()->add_class("bold_label");
		m_endPage.get_style_context()->add_class("assistant_title");
		m_voteQuestionsTitle.get_style_context()->add_class("bold_label");
	}

	std::vector<Gtk::Box> *VoteAssistantWindow::getStartEndPages() {
		return &m_startEndPages;
	}

	std::map<unsigned int, Gtk::Box> *VoteAssistantWindow::getVotePages() {
		return &m_votePages;
	}

	std::vector<Gtk::Label> *VoteAssistantWindow::getQuestions() {
		return &m_questions;
	}

	std::vector<Gtk::Label> *VoteAssistantWindow::getQuestionsOverview() {
		return &m_questionsOverview;
	}

	std::vector<std::map<unsigned int, Gtk::RadioButton>> *VoteAssistantWindow::getVoteOptions() {
		return &m_voteOptions;
	}

	Gtk::Label *VoteAssistantWindow::getOverview() {
		return &m_overview;
	}

	Gtk::Label *VoteAssistantWindow::getVoteQuestionsTitle() {
		return &m_voteQuestionsTitle;
	}

	Gtk::Label *VoteAssistantWindow::getConfirmation() {
		return &m_confirmation;
	}

	Gtk::Label *VoteAssistantWindow::getConfirmationSpinnerText() {
		return &m_confirmationSpinnerText;
	}

	Gtk::Box *VoteAssistantWindow::getConfirmationSummaryBox() {
		return &m_confirmationSummaryBox;
	}

	Gtk::Label *VoteAssistantWindow::getConfirmationSummaryTitle() {
		return &m_confirmationSummaryTitle;
	}

	std::vector<Gtk::Label> *VoteAssistantWindow::getConfirmationSummary() {
		return &m_confirmationSummary;
	}

	Gtk::Label *VoteAssistantWindow::getEndPage() {
		return &m_endPage;
	}

	Gtk::Label *VoteAssistantWindow::getEndPageText() {
		return &m_endPageText;
	}

	Gtk::Label *VoteAssistantWindow::getEndPageSpinnerText() {
		return &m_endPageSpinnerText;
	}

	Gtk::Spinner *VoteAssistantWindow::getSpinVotingPackage() {
		return &m_spinnerVotingPackage;
	}

	Gtk::Spinner *VoteAssistantWindow::getSpinSubmitVotes() {
		return &m_spinnerSubmitVotes;
	}
}