#ifndef MK_TFHE_VOTING_VOTESTATUSWINDOW_H
#define MK_TFHE_VOTING_VOTESTATUSWINDOW_H


#include <mk-tfhe-voting/helpers/votetrackinginformation.h>

#include <gtkmm.h>

#include <memory>
#include <map>
#include <vector>


namespace hsr::votestatuswindow {
	class VoteStatusWindow : public Gtk::Window {

		int window_width{1000};

		int window_height{200};

		Gtk::Grid m_main_grid;

		Gtk::Label m_titleAppLabel;

		Gtk::Grid m_question_grid;

		std::vector<Gtk::Label> m_voter_label;

		std::vector<Gtk::Box> m_bb_box;
		std::vector<Gtk::Label> m_bb_label;
		std::vector<Gtk::Grid> m_bb_grid;

	public:
		explicit VoteStatusWindow();

		Gtk::Grid *getMainGrid();

		Gtk::Label *getTitleAppLabel();

		Gtk::Grid *getQuestionGrid();

		std::vector<Gtk::Label> *getVoterLabel();

		std::vector<Gtk::Box> *getBbBox();

		std::vector<Gtk::Label> *getBbLabel();

		std::vector<Gtk::Grid> *getBbGrid();
	};
}


#endif //MK_TFHE_VOTING_VOTESTATUSWINDOW_H
