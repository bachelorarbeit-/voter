#ifndef MK_TFHE_VOTING_VOTEASSISTANTWINDOW_H
#define MK_TFHE_VOTING_VOTEASSISTANTWINDOW_H


#include "../helpers/genericworker.h"
#include <mk-tfhe-voting/helpers/VotingPackage.h>

#include <gtkmm.h>

#include <vector>
#include <map>


namespace hsr::voteassistantwindow {
	class VoteAssistantWindow : public Gtk::Assistant {

		Gtk::Label m_overview;

		Gtk::Label m_voteQuestionsTitle;

		Gtk::Label m_confirmation;

		Gtk::Label m_confirmationSpinnerText;

		Gtk::Box m_confirmationSummaryBox;

		Gtk::Label m_confirmationSummaryTitle;

		std::vector<Gtk::Label> m_confirmationSummary;

		Gtk::Label m_endPage;

		Gtk::Label m_endPageSpinnerText;

		Gtk::Label m_endPageText;

		std::vector<Gtk::Box> m_startEndPages;

		std::map<unsigned int, Gtk::Box> m_votePages;

		std::vector<Gtk::Label> m_questions;

		std::vector<Gtk::Label> m_questionsOverview;

		std::vector<std::map<unsigned int, Gtk::RadioButton>> m_voteOptions;

		Gtk::Spinner m_spinnerVotingPackage;

		Gtk::Spinner m_spinnerSubmitVotes;

		int window_width{1200};

		int window_height{600};

	public:
		VoteAssistantWindow();

		std::vector<Gtk::Box> *getStartEndPages();

		std::map<unsigned int, Gtk::Box> *getVotePages();

		std::vector<Gtk::Label> *getQuestions();

		std::vector<Gtk::Label> *getQuestionsOverview();

		std::vector<std::map<unsigned int, Gtk::RadioButton>> *getVoteOptions();

		Gtk::Label *getOverview();

		Gtk::Label *getVoteQuestionsTitle();

		Gtk::Label *getConfirmation();

		Gtk::Label *getConfirmationSpinnerText();

		Gtk::Box *getConfirmationSummaryBox();

		Gtk::Label *getConfirmationSummaryTitle();

		std::vector<Gtk::Label> *getConfirmationSummary();

		Gtk::Spinner *getSpinVotingPackage();

		Gtk::Label *getEndPage();

		Gtk::Label *getEndPageText();

		Gtk::Label *getEndPageSpinnerText();

		Gtk::Spinner *getSpinSubmitVotes();
	};
}


#endif //MK_TFHE_VOTING_VOTEASSISTANTWINDOW_H
