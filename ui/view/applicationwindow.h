#ifndef VOTER_APPLICATIONWINDOW_H
#define VOTER_APPLICATIONWINDOW_H

#include <voter/Voter.h>

#include <gtkmm.h>

#include <string>
#include <vector>
#include <thread>
#include <memory>
#include <optional>
#include <functional>


namespace hsr::applicationwindow {
	class ApplicationWindow : public Gtk::ApplicationWindow {
		int window_width{1200};
		int window_height{500};

		Gtk::Grid m_mainGrid;

		Gtk::Label m_titleAppLabel;

		Gtk::Label m_coordinatorUri;

		Gtk::Label m_titleElectionsLabel;
		Gtk::Box m_questionBox;
		std::map<unsigned int, Gtk::CheckButton> m_questions;

		Gtk::Box m_startVoteButtonBox;
		Gtk::Label m_startVoteButtonServerStatus;
		Gtk::Button m_startVoteButton;

		Gtk::Box m_refreshBox;
		Gtk::Button m_refreshVotesButton;
		Gtk::Label m_refreshTitleLabel;
		Gtk::Label m_refreshTimeLabel;
		Gtk::Box m_statusVotesButtonBox;
		Gtk::Label m_statusVotesButtonServerStatus;
		Gtk::Button m_statusVotesButton;

		Gtk::Button m_buttonQuit;

		std::optional<std::function<void()>> startVote;
		std::optional<std::function<void()>> statusVote;
		std::optional<std::function<void()>> refresh;

		void onStartCallback();

		void onStatusCallback();

		void onRefreshCallback();

	public:
		explicit ApplicationWindow();

		void on_button_quit();

		void setOnStartVote(const std::function<void()> &onStartVote);

		void setOnStatusVotes(const std::function<void()> &onStatusVote);

		void setOnRefreshVote(const std::function<void()> &onRefresh);

		Gtk::Grid *getMainGrid();

		Gtk::Label *getCoordinatorUri();

		Gtk::Button *getStartVoteButton();

		Gtk::Label *getRefreshTimeLabel();

		Gtk::Button *getRefreshVotesButton();

		Gtk::Box *getQuestionBox();

		std::map<unsigned int, Gtk::CheckButton> *getQuestions();

		Gtk::Label *getStartVoteButtonServerStatus();

		Gtk::Label *getStatusVotesButtonServerStatus();
	};
}

#endif //VOTER_APPLICATIONWINDOW_H
