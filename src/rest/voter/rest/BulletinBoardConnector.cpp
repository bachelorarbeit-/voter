#include <mk-tfhe-voting/exceptions/BulletinBoardError.h>

#include "BulletinBoardConnector.h"

namespace hsr::voteclient {
	BulletinBoardConnector::BulletinBoardConnector(const web::uri &base_uri) : client{base_uri} {}

	void BulletinBoardConnector::submitVote(unsigned int id, std::vector<std::shared_ptr<MKLweSample>> vote) {
		std::cout << "submitVote called" << std::endl;

		web::json::value body = getVoteJSON(vote, id);

		web::http::http_request httpRequest{web::http::methods::POST};
		httpRequest.set_body(body);
		httpRequest.set_request_uri(web::uri_builder("/vote").to_uri());

		auto res = client.request(httpRequest).get();
		if (res.status_code() != web::http::status_codes::OK) {
			throw BulletinBoardError("Couldn't send vote to bulletin boards.");
		}
	}

	web::json::value BulletinBoardConnector::getVoteJSON(const std::vector<std::shared_ptr<MKLweSample>> &votes, unsigned ballotId) {
		web::json::value body = web::json::value::object();
		web::json::value ballot = web::json::value::array();
		for (std::size_t i{}; i < votes.size(); ++i) {
			const std::shared_ptr<MKLweSample> &vote = votes.at(i);
			web::json::value sample = web::json::value::object();

			for (int j{}; j < vote->n * vote->parties; ++j) {
				sample["phaseShift"][j] = web::json::value::number(vote->a[j]);
			}
			sample["phase"] = web::json::value::number(vote->b);
			sample["current variance"] = web::json::value::number(vote->current_variance);
			ballot[i] = sample;
		}

		body["clientversion"] = web::json::value::number(1);
		body["ballot"] = ballot;
		body["ballotId"] = ballotId;

		return body;
	}
}