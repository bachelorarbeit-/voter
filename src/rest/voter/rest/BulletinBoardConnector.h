#ifndef MK_TFHE_VOTING_BULLETINBOARDCONNECTOR_H
#define MK_TFHE_VOTING_BULLETINBOARDCONNECTOR_H

#include <mk-tfhe-voting/helpers/IBulletinBoardForVoter.h>
#include <cpprest/http_client.h>

namespace hsr::voteclient {
	class BulletinBoardConnector : public IBulletinBoardForVoter {
		web::http::client::http_client client;

	public:
		explicit BulletinBoardConnector(const web::uri &base_uri);

		void submitVote(unsigned int id, std::vector<std::shared_ptr<MKLweSample>> vote) override;

		static web::json::value getVoteJSON(const std::vector<std::shared_ptr<MKLweSample>> &vote, unsigned ballotId);
	};
}

#endif //MK_TFHE_VOTING_BULLETINBOARDCONNECTOR_H
