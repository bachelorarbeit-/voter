#ifndef MK_TFHE_VOTING_PARAMSFROMJSON_H
#define MK_TFHE_VOTING_PARAMSFROMJSON_H


#include <mk-tfhe-voting/helpers/Params.h>

#include <cpprest/http_client.h>


static Params getParamsFromJson(web::json::value &paramsJson) {
	unsigned trustees = paramsJson.at("trustees").as_number().to_uint32();
	unsigned bulletinBoards = paramsJson.at("bulletinBoards").as_number().to_uint32();
	unsigned choices = paramsJson.at("counterLength").as_number().to_uint32();
	int32_t k = paramsJson.at("k").as_number().to_int32();
	double stdevKs = paramsJson.at("ksStdev").as_number().to_double();
	double stdevBk = paramsJson.at("bkStdev").as_number().to_double();
	int32_t n = paramsJson.at("sn").as_number().to_int32();
	double stdevLwe = paramsJson.at("stdevLWE").as_number().to_double();
	uint32_t bksbit = paramsJson.at("bksBit").as_number().is_uint32();
	int32_t ksDimension = paramsJson.at("dks").as_number().to_int32();
	int32_t N = paramsJson.at("ln").as_number().to_int32();
	double stdevRlweKey = paramsJson.at("stdevRLWEkey").as_number().to_double();
	double stdevRlwe = paramsJson.at("stdevRLWE").as_number().to_double();
	double stdevRgsw = paramsJson.at("stdevRGSW").as_number().to_double();
	int32_t bgbit = paramsJson.at("bgBit").as_number().to_int32();
	int32_t gDimension = paramsJson.at("dg").as_number().to_int32();
	unsigned generatedKeys = paramsJson.at("generatedKeys").as_number().to_uint32();
	unsigned encryptionKeys = paramsJson.at("encryptionKeys").as_number().to_uint32();


	return Params{k, n, stdevLwe, bksbit, ksDimension, stdevKs, N, stdevRlweKey, stdevRlwe, stdevRgsw,
				  bgbit, gDimension, stdevBk, trustees, generatedKeys, encryptionKeys, bulletinBoards, choices};
}


#endif //MK_TFHE_VOTING_PARAMSFROMJSON_H
