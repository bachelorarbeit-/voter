#include <mkTFHEsamples.h>

#include "VoterClient.h"
#include "ParamsFromJson.h"
#include "../data/voterpersistence.h"

#include <mk-tfhe-voting/exceptions/CoordinatorError.h>
#include <mk-tfhe-voting/exceptions/JsonError.h>
#include "../exceptions/NotRegisteredError.h"

#include <stdexcept>


namespace hsr::voteclient {
	VoterClient::VoterClient(const web::uri &base_uri) : client{base_uri} {}

	web::http::http_response VoterClient::getRequest(const web::uri &uri) {
		web::http::http_request httpRequest{web::http::methods::GET};
		httpRequest.headers().add("Authorization", apiKey);
		httpRequest.set_request_uri(uri);
		auto httpResponse = client.request(httpRequest).get();
		if (httpResponse.status_code() != web::http::status_codes::OK) {
			throw CoordinatorError("Coordinator status code " + std::to_string(httpResponse.status_code()));
		}
		return httpResponse;
	}

	void VoterClient::registerVoter() {
		hsr::voterpersistence::VoterPersistence vp{};

		apiKey = vp.readApiKey();

		if (apiKey.empty()) {
			auto json = getRequest("/voter/register").extract_json().get();
			apiKey = "Bearer " + json.at("apiKey").as_string();
			vp.writeApiKey(apiKey);
		}
	}

	std::map<unsigned int, VotesText> VoterClient::getVotesVoter() {
		auto json = getRequest("/voter/getVotes").extract_json().get();

		std::map<unsigned int, VotesText> votesInfo{};
		for (auto &vote : json.as_array()) {
			std::vector<std::string> ch;
			for (auto &choice : vote.at("choices").as_array()) {
				ch.emplace_back(choice.at("option").as_string());
			}
			unsigned int id{vote.at("voteId").as_number().to_uint32()};
			votesInfo.insert({id, VotesText{id, vote.at("question").as_string(), ch}});
		}
		return votesInfo;
	}

	std::shared_ptr<VotingPackage> VoterClient::getVotingPackage(unsigned voteId) {
		auto json = getRequest("/voter/votingpackage/" + std::to_string(voteId)).extract_json().get();

		std::shared_ptr<VotingPackage> votingPackage;
		std::shared_ptr<std::vector<std::shared_ptr<LweSample>>> publicKeys{std::make_shared<std::vector<std::shared_ptr<LweSample>>>()};
		Params params{};
		VotesText votesText{};
		std::tm voteStart{};
		std::tm voteEnd{};
		unsigned ballotId{};

		std::vector<std::shared_ptr<BulletinBoardConnector>> bulletinBoardConnectors{};

		auto bb = json.at("bulletinBoards").as_array();
		for (std::size_t i{}; i < bb.size(); ++i) {
			web::uri_builder uriBuilder{};
			uriBuilder.set_host(bb.at(i).at("host").as_string());
			uriBuilder.set_port(bb.at(i).at("port").as_number().to_int32());
			bulletinBoardConnectors.emplace_back(std::make_shared<BulletinBoardConnector>(uriBuilder.to_uri()));
		}

		votesText.uniqueIdentifier = json.at("voteId").as_number().to_int32();
		votesText.question = json.at("question").as_string();
		for (auto &choice : json.at("choices").as_array()) {
			votesText.choices.emplace_back(choice.at("option").as_string());
		}

		web::json::value paramsJson = json.at("params");
		params = getParamsFromJson(paramsJson);

		std::string formatString("%Y-%m-%dT%H:%M:%S");
		std::string locale("en_US.utf-8");

		std::string voteStartString = paramsJson.at("voteStart").as_string();
		std::istringstream voteStartStream(voteStartString);
		voteStartStream.imbue(std::locale(locale));
		voteStartStream >> std::get_time(&voteStart, formatString.c_str());

		std::string voteEndString = paramsJson.at("voteEnd").as_string();
		std::istringstream voteEndStream(voteEndString);
		voteEndStream.imbue(std::locale(locale));
		voteEndStream >> std::get_time(&voteEnd, formatString.c_str());

		if (voteStartStream.fail() || voteEndStream.fail()) {
			std::cout << "Couldn't parse start or/and end time correctly.\n";
		}

		web::json::value publicKeyJson = json.at("trustees");

		for (std::size_t i{}; i < params.trustees; ++i) {
			auto samplesJson = publicKeyJson.at(std::to_string(i)).as_array();
			for (auto &sampleJson : samplesJson) {
				auto sample = std::make_shared<LweSample>(params.lweParams.get());
				auto a = sampleJson.at("phaseShift").as_array();
				for (std::size_t j{}; j < a.size(); ++j) {
					sample->a[j] = a.at(j).as_number().to_int32();
				}
				sample->b = sampleJson.at("phase").as_number().to_int32();
				publicKeys->push_back(sample);
			}
		}
		ballotId = json.at("voteId").as_number().to_uint32();

		std::vector<std::weak_ptr<IBulletinBoardForVoter>> bulletinBoards{bulletinBoardConnectors.begin(), bulletinBoardConnectors.end()};
		this->bulletinBoardConnectors.insert(this->bulletinBoardConnectors.end(), bulletinBoardConnectors.begin(), bulletinBoardConnectors.end());

		return std::make_shared<VotingPackage>(ballotId, bulletinBoards, publicKeys, params, votesText, voteStart, voteEnd);
	}

	std::vector<unsigned int> VoterClient::getParticipations() {
		if (apiKey.empty()) {
			throw NotRegisteredError{"The api key is wrong or you are not registered."};
		}

		auto json = getRequest("/voter/participations").extract_json().get();

		std::vector<unsigned int> participations{};

		std::map<unsigned int, VotesText> votesInfo{};
		for (auto &vote : json.as_array()) {
			participations.emplace_back(vote.at("voteId").as_number().to_uint32());
		}
		return participations;
	}

	std::map<unsigned int, VoteStatusInformation> VoterClient::getVoteStatusInformation(std::vector<unsigned int> &voteIds) {
		std::map<unsigned int, VoteStatusInformation> voteTrack{};

		for (unsigned int &id : voteIds) {
			auto json = getRequest("/voter/" + std::to_string(id) + "/getState").extract_json().get();
			if (json.size() == 0) {
				throw JsonError("There is no voting status available.");
			}

			auto statusJsonArray = json.as_array();

			bool packageReceivedVoter{false};
			bool packageSentVoter{false};

			bool voteReceivedBulletinBoard{false};
			bool voteDequeuedBulletinBoard{false};
			bool voteProcessedBulletinBoard{false};

			std::map<unsigned int, VoteStatusInformation::BulletinBoard> bulletinBoardIdBulletinBoard;

			int previousBulletinBoardNr{1};
			int bulletinBoardCurrentIdNr{};

			for (auto &statusObject : statusJsonArray) {
				auto status = statusObject.as_object();

				if (status.at("origin").as_string() == "Voter") {
					if (status.at("type").is_string() && status.at("type").as_string() == "packageReceived") {
						packageReceivedVoter = true;
					} else if (status.at("type").is_string() && status.at("type").as_string() == "voteSent") {
						packageSentVoter = true;
					}
				} else if (status.at("origin").as_string() == "BulletinBoard") {
					bulletinBoardCurrentIdNr = status.at("bulletinboardId").as_number().to_int32();

					if (bulletinBoardIdBulletinBoard.find(bulletinBoardCurrentIdNr) == bulletinBoardIdBulletinBoard.end()) {
						bulletinBoardIdBulletinBoard.insert({bulletinBoardCurrentIdNr, VoteStatusInformation::BulletinBoard(false, false, false)});
					}

					if (bulletinBoardCurrentIdNr != previousBulletinBoardNr) {
						bulletinBoardIdBulletinBoard.at(bulletinBoardCurrentIdNr).status.at("voteReceived") = voteReceivedBulletinBoard;
						bulletinBoardIdBulletinBoard.at(bulletinBoardCurrentIdNr).status.at("voteDequeued") = voteDequeuedBulletinBoard;
						bulletinBoardIdBulletinBoard.at(bulletinBoardCurrentIdNr).status.at("voteProcessed") = voteProcessedBulletinBoard;

						voteReceivedBulletinBoard = false;
						voteDequeuedBulletinBoard = false;
						voteProcessedBulletinBoard = false;
					}

					if (status.at("type").is_string() && status.at("type").as_string() == "voteReceived") {
						voteReceivedBulletinBoard = true;
					} else if (status.at("type").is_string() && status.at("type").as_string() == "voteDequeued") {
						voteDequeuedBulletinBoard = true;
					} else if (status.at("type").is_string() && status.at("type").as_string() == "voteProcessed") {
						voteProcessedBulletinBoard = true;
					}

					previousBulletinBoardNr = bulletinBoardCurrentIdNr;
				}
			}

			if (!bulletinBoardIdBulletinBoard.empty()) {
				bulletinBoardIdBulletinBoard.at(bulletinBoardCurrentIdNr).status.at("voteReceived") = voteReceivedBulletinBoard;
				bulletinBoardIdBulletinBoard.at(bulletinBoardCurrentIdNr).status.at("voteDequeued") = voteDequeuedBulletinBoard;
				bulletinBoardIdBulletinBoard.at(bulletinBoardCurrentIdNr).status.at("voteProcessed") = voteProcessedBulletinBoard;
			}

			voteTrack.insert({id, VoteStatusInformation{id, packageReceivedVoter, packageSentVoter, bulletinBoardIdBulletinBoard}});
		}

		return voteTrack;
	}

	void VoterClient::trackVoteReceivedPackage(std::vector<unsigned int> const &voteIds) {
		trackVote(voteIds, "packageReceived");
	}

	void VoterClient::trackVoteSentVote(std::vector<unsigned int> const &voteIds) {
		trackVote(voteIds, "voteSent");
	}

	void VoterClient::trackVote(std::vector<unsigned int> const &voteIds, std::string const &type) {
		if (!voteIds.empty()) {
			for (unsigned int voteId : voteIds) {
				web::json::value body = web::json::value::object();
				body["voteId"] = web::json::value::number(voteId);
				body["type"] = web::json::value::string(type);

				postRequest("/voter/trackVote", body);
			}
		}
	}

	web::http::http_response VoterClient::postRequest(const web::uri &uri, const web::json::value &body) {
		web::http::http_request httpRequest{web::http::methods::POST};
		httpRequest.headers().add("Authorization", apiKey);
		httpRequest.set_request_uri(uri);
		httpRequest.set_body(body);
		auto httpResponse = client.request(httpRequest).get();
		if (httpResponse.status_code() != web::http::status_codes::OK) {
			throw CoordinatorError("Coordinator status code " + std::to_string(httpResponse.status_code()));
		}
		return httpResponse;
	}
}