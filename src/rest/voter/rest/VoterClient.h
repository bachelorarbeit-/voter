#ifndef MK_TFHE_VOTING_VOTERCLIENT_H
#define MK_TFHE_VOTING_VOTERCLIENT_H


#include <mk-tfhe-voting/helpers/ICoordinatorForVoter.h>
#include <mk-tfhe-voting/helpers/IBulletinBoardForVoter.h>
#include <mk-tfhe-voting/helpers/Params.h>
#include "BulletinBoardConnector.h"

#include <cpprest/http_client.h>
#include <mk-tfhe-voting/helpers/votetrackinginformation.h>

#include <vector>
#include <map>


namespace hsr::voteclient {
	class VoterClient : public ICoordinatorForVoter {

		web::http::client::http_client client;

		std::string apiKey{""};

		web::http::http_response getRequest(const web::uri &uri);

		web::http::http_response postRequest(const web::uri &uri, const web::json::value &body);

		void trackVote(std::vector<unsigned int> const &voteIds, std::string const &type);

		std::vector<std::shared_ptr<BulletinBoardConnector>> bulletinBoardConnectors{};

	public:
		explicit VoterClient(const web::uri &base_uri);

		void registerVoter() override;

		std::map<unsigned int, VotesText> getVotesVoter() override;

		std::shared_ptr<VotingPackage> getVotingPackage(unsigned voteId) override;

		std::vector<unsigned int> getParticipations() override;

		std::map<unsigned int, VoteStatusInformation> getVoteStatusInformation(std::vector<unsigned int> &voteIds) override;

		void trackVoteReceivedPackage(std::vector<unsigned int> const &voteIds) override;

		void trackVoteSentVote(std::vector<unsigned int> const &voteIds) override;
	};
}


#endif //MK_TFHE_VOTING_VOTERCLIENT_H
