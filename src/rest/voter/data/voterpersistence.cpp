#include "voterpersistence.h"

#include <filesystem>
#include <iostream>
#include <stdexcept>

#include "../exceptions/ApiKeyError.h"


namespace hsr::voterpersistence {
	VoterPersistence::VoterPersistence() {
		if (!std::filesystem::exists(fileName)) {
			std::cout << "Created new key file\n";
			std::ifstream input{fileName};
			input.close();
		}
	}

	std::string VoterPersistence::readApiKey() {
		std::string readString{};
		std::ifstream input{fileName};
		std::getline(input, readString);

		if (!readString.empty()) {
			unsigned int apiKeyLength{71};
			if (input.fail() || readString.size() != apiKeyLength) {
				throw ApiKeyError("Couldn't read API Key properly, delete the API file.");
			}
		}

		input.close();

		return readString;
	}

	bool VoterPersistence::writeApiKey(const std::string &apiKey) {
		std::ofstream output{fileName};
		output << apiKey;
		return output.good();
	}
}
