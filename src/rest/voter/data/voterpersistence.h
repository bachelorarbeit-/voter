#ifndef MK_TFHE_VOTING_VOTERPERSISTENCE_H
#define MK_TFHE_VOTING_VOTERPERSISTENCE_H


#include <string>
#include <fstream>


namespace hsr::voterpersistence {
	class VoterPersistence {

		std::string fileName{"apiKey.txt"};

	public:
		VoterPersistence();

		std::string readApiKey();

		bool writeApiKey(const std::string &apiKey);
	};
}


#endif //MK_TFHE_VOTING_VOTERPERSISTENCE_H
