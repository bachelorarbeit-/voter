#ifndef MK_TFHE_VOTING_API_KEY_ERROR_H
#define MK_TFHE_VOTING_API_KEY_ERROR_H


#include <mk-tfhe-voting/exceptions/Exception.h>

struct ApiKeyError : Exception {
	ApiKeyError(const std::string &message, const exception &cause) : Exception(message, cause) {}

	explicit ApiKeyError(const std::string &message) : Exception(message) {}
};


#endif //MK_TFHE_VOTING_API_KEY_ERROR_H
