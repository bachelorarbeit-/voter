#ifndef MK_TFHE_VOTING_NOT_REGISTERED_ERROR_H
#define MK_TFHE_VOTING_NOT_REGISTERED_ERROR_H


#include <mk-tfhe-voting/exceptions/Exception.h>

struct NotRegisteredError : Exception {
	NotRegisteredError(const std::string &message, const exception &cause) : Exception(message, cause) {}

	explicit NotRegisteredError(const std::string &message) : Exception(message) {}
};


#endif //MK_TFHE_VOTING_NOT_REGISTERED_ERROR_H
