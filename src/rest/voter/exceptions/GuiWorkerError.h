#ifndef MK_TFHE_VOTING_GUI_WORKER_ERROR_H
#define MK_TFHE_VOTING_GUI_WORKER_ERROR_H


#include <mk-tfhe-voting/exceptions/Exception.h>


struct GuiWorkerError : Exception {
	GuiWorkerError(const std::string &message, const exception &cause) : Exception(message, cause) {}

	explicit GuiWorkerError(const std::string &message) : Exception(message) {}
};


#endif //MK_TFHE_VOTING_GUI_WORKER_ERROR_H
