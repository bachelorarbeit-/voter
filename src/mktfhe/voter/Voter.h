#ifndef MK_TFHE_VOTING_VOTER_H
#define MK_TFHE_VOTING_VOTER_H

#include <mk-tfhe-voting/helpers/ICoordinatorForVoter.h>
#include <mk-tfhe-voting/helpers/IBulletinBoardForVoter.h>

#include <vector>


namespace hsr::voter {
	class Voter {
		std::weak_ptr<ICoordinatorForVoter> coordinator;

		std::map<unsigned, std::shared_ptr<VotingPackage>> votingPackages;

		std::vector<std::shared_ptr<MKLweSample>>
		encryptVote(const std::vector<bool> &unencryptedVote, unsigned int election);

		static std::vector<bool> createVote(unsigned int numberOfChoices, unsigned int choice);

	public:
		explicit Voter(std::weak_ptr<ICoordinatorForVoter> coordinator);

		void vote();

		void vote(std::vector<unsigned int> electionNumber, std::vector<unsigned int> choices);

		const std::weak_ptr<ICoordinatorForVoter> &getCoordinator() const;

		void setVotingPackage(const std::map<unsigned, std::shared_ptr<VotingPackage>> &votingPackage);

		std::map<unsigned int, std::shared_ptr<VotingPackage>> getVotingPackages(const std::vector<unsigned> &voteIds);
	};
}


#endif //MK_TFHE_VOTING_VOTER_H
