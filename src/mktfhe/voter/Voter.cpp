#include "Voter.h"

#include <mk-tfhe-voting/helpers/Encryption.h>
#include <lwesamples.h>

#include <memory>
#include <vector>
#include <map>
#include <cmath>


namespace hsr::voter {
	Voter::Voter(std::weak_ptr<ICoordinatorForVoter> coordinator) : coordinator{std::move(coordinator)} {}

	std::map<unsigned int, std::shared_ptr<VotingPackage>> Voter::getVotingPackages(const std::vector<unsigned> &voteIds) {
		auto lockedCoordinator{coordinator.lock()};
		for (auto &voteId : voteIds) {
			std::shared_ptr<VotingPackage> votingPackage{lockedCoordinator->getVotingPackage(voteId)};
			votingPackages.insert(std::make_pair(voteId, votingPackage));
		}
		return votingPackages;
	}

	std::vector<std::shared_ptr<MKLweSample>> Voter::encryptVote(const std::vector<bool> &unencryptedVote, unsigned int election) {
		std::vector<std::shared_ptr<MKLweSample>> encryptedVote{};
		auto &votingParams{this->votingPackages.at(election)};
		for (bool unencryptedBit : unencryptedVote) {
			auto encryptedBit{std::make_shared<MKLweSample>(votingParams->lweParams.get(), votingParams->mkParams.get())};
			hsr::helpers::mySymEncrypt(encryptedBit.get(), unencryptedBit, votingParams->generatedKeys, votingParams->encryptionKeys, *votingParams->lwePubKey);
			encryptedVote.push_back(encryptedBit);
		}
		return encryptedVote;
	}

	void Voter::vote() {
		std::vector<unsigned int> electionNumber{0};
		std::vector<unsigned int> choices{rand() % votingPackages.at(electionNumber.at(0))->choices};
		this->vote(electionNumber, choices);
	}

	void Voter::vote(std::vector<unsigned int> electionNumber, std::vector<unsigned int> choices) {
		for (std::size_t election{0}; election < electionNumber.size(); ++election) {
			auto &votingPackage = *votingPackages.at(electionNumber.at(election));
			std::vector<bool> unencryptedVote{createVote(votingPackage.choices, choices.at(election))};
			std::vector<std::shared_ptr<MKLweSample>> encryptedVote{encryptVote(unencryptedVote, electionNumber.at(election))};
			for (auto &bulletinBoard : votingPackage.bulletinBoards) {
				bulletinBoard.lock()->submitVote(votingPackage.ballotId, encryptedVote);
			}
		}
	}

	std::vector<bool> Voter::createVote(unsigned int numberOfChoices, unsigned int choice) {
		unsigned int voteBits{(unsigned int) std::ceil(std::log2(numberOfChoices))};
		unsigned int selectedVote{choice % numberOfChoices};
		std::vector<bool> randomVote(voteBits, false);
		for (unsigned int i{0}; i < voteBits; i++) {
			randomVote.at(i) = selectedVote & (1u << i);
		}
		return randomVote;
	}

	const std::weak_ptr<ICoordinatorForVoter> &Voter::getCoordinator() const {
		return coordinator;
	}

	void Voter::setVotingPackage(const std::map<unsigned, std::shared_ptr<VotingPackage>> &votingPackage) {
		this->votingPackages = votingPackage;
	}
}
